using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Util;
using Java.IO;
using Wikitude.Architect;

namespace Com.Wikitude.Samples
{
	[Activity (Label = "Wikitude Samples", MainLauncher = true)]
	public class MainActivity : ListActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.list_startscreen);


			

			
            this.StartActivity(typeof(BasicArchitectActivity));
        }

	
	}
}


